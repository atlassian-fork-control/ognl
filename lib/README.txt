Library Dependencies
====================
The following packages are required by OGNL at build time.  They are
not required to run OGNL or be distributed along with OGNL.

The Version is the version that OGNL has been tested with.  If you
deviate from this you do so with the knowledge that you may have to
alter the build scripts or otherwise figure out a problem.  On the other
hand it may work out just fine.

The usage of the following jars are govered by the build.properties
file.  This is where locations of external tools can be configured.

All of the jars listed here normally will live in the "lib" directory
and the default build.properties references them there already, so this
would be the quickest way to get it running.  The docbook distributions
could possible live under lib, but the default build.properties
references them externally and the locations must be changed in that
file.

Note  Package              Version   Download Location
----  -------              -------   -----------------
  1   ant-contrib-0.3.jar      0.3   http://ant-contrib.sourceforge.net
  2   javacc.jar               2.1+  http://javacc.dev.java.net
  3   docbook-xml              4.2   http://www.oasis-open.org/docbook/xml
  3   docbook-xsl           1.65.0   http://docbook.sourceforge.net
  4   junit.jar              3.8.1   http://www.junit.org
  5   javassist.jar          2.6.0   http://www.jboss.org/developers/projects/javassist.html


(1) These jars are required to build OGNL and the JavaDoc documentation.
(2) JavaCC comes in several publicly released versions, each with a
    different package name for the tools.  By default the build.properties
    file specifies the 3.2 version.  The build file will then read
    properties from the javacc-<version>.properties.  If a different
    version is used then the build.properties file needs to be
    altered to reflect that.  IT IS RECOMMENDED THAT YOU GET THE LATEST
    VERSION OF JAVACC.
(3) The docbook distributions are only required if you wish to build
    the DocBook-based documentation.  You must get the entire XML or
    XSL distribution.
(4) The JUnit jar is needed to run the tests and must be installed in
    the Ant installation's lib directory.
(5) Javassist is only required for running the tests because of the
    org.ognl.test.CompilingPropertyAccessor.
